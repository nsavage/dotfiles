import std.file;
import std.path;
import std.process;
import std.stdio;

string basepath = "/tmp/test";
const string FOLDER = "/home/media/books/toadd";
const string CALIBRE_DATABASE = "/home/media/books/calibre";
const string CALIBREDB = "/usr/bin/calibredb";
const string LIBRARY = "/home/media/books/calibre";

const string EBOOK_CONVERT = "/usr/bin/ebook-convert";

string get_output_path(string filename) {
    return buildPath([basepath, filename]);
}

void create_output_folder(string filename) {
    auto new_folder = get_output_path(filename);
    mkdir(new_folder);
}

void process_epub(DirEntry file) {
    string filename = file.baseName().stripExtension();
    string base_filename = filename.stripExtension();
    writeln(base_filename);
    create_output_folder(base_filename);
}

void process_other(DirEntry file) {
    string filename = file.baseName();
    string base_filename = filename.stripExtension();
    create_output_folder(base_filename);
    string new_file = buildPath([basepath, base_filename, filename]);
    writeln(execute(["mv", file, new_file]));
}

void add_to_calibre(string output_folder) {
    execute([CALIBREDB, "add", "-1", "--with-library", LIBRARY, output_folder]);
}

void main() {
    DirIterator files = dirEntries(basepath, SpanMode.shallow);
    string[] outputFolder;
    foreach(file; files) {
	if (file.isFile()) {
	    string extension = file.baseName().extension();
	    if (extension == ".epub") {
		process_epub(file);
		outputFolder ~= file.baseName().stripExtension();
	    }
	    else if (extension == ".mobi") {
		process_other(file);
		outputFolder ~= file.baseName().stripExtension();
	    }
	    else if (extension == ".azw3") {
		process_other(file);
		outputFolder ~= file.baseName().stripExtension();
	    }
	    else if (extension == ".pdf") {
		process_other(file);
		outputFolder ~= file.baseName().stripExtension();
	    }
	    // writeln(file.baseName());
	}
    }
    foreach(folder; outputFolder) {
	writeln(folder);
    }
}

;------------------------------------
;; Nick Savage's .emacs file
;------------------------------------

;;; Code

(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(eval-when-compile
  (require 'use-package))

(add-to-list 'load-path "~/code/org-mode/lisp/")
(add-to-list 'load-path "~/code/org-mode/contrib/lisp/")
(setq user-mail-address	"nick@nicksavage.ca"
      user-full-name	"Nick Savage")

(use-package evil
  :ensure t
  :init
  (setq evil-want-C-i-jump nil)
  (evil-mode t)
  :config
  (add-to-list 'evil-emacs-state-modes 'vterm-mode)
  (add-to-list 'evil-emacs-state-modes 'nov-mode)
  (add-to-list 'evil-emacs-state-modes 'elfeed-search-mode)
  (add-to-list 'evil-emacs-state-modes 'dired-mode)
  (add-to-list 'evil-emacs-state-modes 'elfeed-show-mode))

(use-package ivy
  :ensure t
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) "))

;; (use-package org-roam
;;   :load-path "~/code/org-roam"
;;   :ensure t
;;   :hook
;;   (after-init . org-roam-mode)
;;   :custom
;;   (org-roam-directory "~/notes")
;;   :bind (:map org-roam-mode-map
;;               (("C-c n l" . org-roam)
;;                ("C-c n f" . org-roam-find-file)
;; 	       ("C-c n d" . org-roam-dailies-find-today)
;;                ("C-c n g" . org-roam-graph))
;;               :map org-mode-map
;;               (("C-c n i" . org-roam-insert))
;;               (("C-c n I" . org-roam-insert-immediate))))

;; (use-package ox-rss
;;   :load-path "~/code/emacs-packages/ox-rss")

(use-package swiper
  :ensure t
  :after ivy)

(use-package counsel
  :ensure t
  :after ivy)

(use-package vterm
  :ensure t)

(use-package company
  :ensure t)

(use-package magit
  :ensure t
  :defer)

(use-package yasnippet
  :ensure t
  :defer)

(use-package markdown-mode
  :ensure t
  :defer)

(use-package elfeed
  :ensure t
  :defer)

(use-package elfeed-org
  :after elfeed
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org"))
  (setq-default elfeed-search-filter "@1-week-ago +unread "))

(use-package projectile
  :load-path "~/code/projectile"
  :ensure t
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package tide
  :config
  
  (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
  (add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
  (add-hook 'typescript-mode-hook #'setup-tide-mode))

(add-to-list 'load-path "~/code/software/emacs-packages/beancount-mode/")
(use-package beancount
  :config
  (add-to-list 'auto-mode-alist '("\\.beancount\\'" . beancount-mode)))

(use-package d-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.d\\'" . d-mode)))

(load "~/.emacs.d/notes.el")

(column-number-mode t)
(display-line-numbers-mode t)
(electric-pair-mode t)
(global-visual-line-mode t)
(line-number-mode t)
(menu-bar-mode -1)
(show-paren-mode t)
(tool-bar-mode -1)

(setq backup-directory-alist '(("." . "~/.saves")))
(setq sgml-basic-offset 4)
(setq-default c-basic-offset 4)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (sqlite . t)
   (latex . t)
   (sql . t)
   (shell . t)
   (calc . t)
   (C . t)))

;; Dired commands

(require 'dired-x)

;; Allow ommiting dotfiles from listing.
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

;; Open dired in current file's directory.
(global-set-key (kbd "C-c C-d") 'dired-jump)

;; Auto load extra dired features.
(add-hook 'dired-load-hook (lambda () (load "dired-x")))

;; Make dired comfy with easy keybindingns & watnot.
(add-hook 'dired-mode-hook
          (lambda ()
            (local-set-key (kbd "/") 'dired-omit-mode)
            (local-set-key (kbd "h") 'dired-hide-details-mode)
            (local-set-key (kbd "b") 'dired-up-directory)
            (local-set-key (kbd "e") 'dired-do-async-shell-command)
            (dired-hide-details-mode 1)
            (dired-omit-mode 1)))

;; elfeed helper functions

; The issue these are trying to solve is that I often am using elfeed
; over a terminal on another computer. Elfeed by default has 'b' which
; opens the entry link in the browser, but if I'm on a terminal this
; opens it on the computer that is actually running the emacs
; daemon. I don't want that! So these will display the url so I can
; copy it.

(defun nsavage-display-elfeed-url ()
  "Display the entry url in an elfeed-search buffer"
  (interactive)
  (let* ((entries (elfeed-search-selected))
	 (links (mapcar #'elfeed-entry-link entries))
	 (links-str (mapconcat #'identity links " ")))
    (message "%s" links-str)))

(defun nsavage-display-elfeed-show-url ()
  "Display the entry url in an elfeed-show buffer"
  (interactive)
  (let ((url (elfeed-entry-link elfeed-show-entry)))
    (message "%s" url)))

(add-hook 'elfeed-search-mode-hook
	  (lambda ()
	    (local-set-key (kbd "l") 'nsavage-display-elfeed-url)))
(add-hook 'elfeed-show-mode-hook
	  (lambda ()
	    (local-set-key (kbd "l") 'nsavage-display-elfeed-show-url)))
(global-set-key [f7] 'elfeed)


;; Other keybindings

(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-c k") 'compile)

(defun nsavage-load-new-vterm ()
  (interactive)
  (let ((current-prefix-arg t))
    (call-interactively #'vterm)))

(global-set-key (kbd "C-c t") 'nsavage-load-new-vterm)
(global-set-key (kbd "C-s") 'swiper-isearch)
(global-set-key (kbd "<mouse-2>") 'clipboard-yank)

(add-hook 'after-init-hook 'global-company-mode)

;; Auto save all buffers when frame loses focus
(add-hook 'focus-out-hook (lambda () (save-some-buffers t)))

;; (load-file "~/.emacs.d/elfeed.el")
;(load-file "~/.emacs.d/websites.el")
(set-face-attribute 'default nil :height 100)

(defun word-count ()
  "Counts the number of words in buffer." 
  (interactive)
  (shell-command-on-region (point-min) (point-max) "wc -w"))

(global-set-key (kbd "C-c d") 'nsavage-open-current-logbook-date)
(global-set-key (kbd "C-c D") 'nsavage-open-given-logbook-date)
(global-set-key (kbd "C-c e") 'nsavage-open-current-todos)
(global-set-key (kbd "C-c n") 'nsavage-open-daily-note)
(global-set-key (kbd "C-c i") 'nsavage-open-inbox-file)
(global-set-key (kbd "C-c f") 'nsavage-grep-notes-directory)

;; org mode keybindings
(global-set-key (kbd "C-c b") (lambda () (interactive) (org-capture nil "e")))
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c C-l") 'org-insert-link)
(global-set-key (kbd "C-c p") 'org-do-demote)
(global-set-key (kbd "C-c P") 'org-do-promote)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c C-v") 'flycheck-list-errors)

(setq org-refile-targets '((nsavage-logbook-filename :maxlevel . 4)))
(setq org-directory "~/notes")
(setq org-agenda-files (list nsavage-logbook-filename))
(setq org-capture-templates
      '(("e" "Elfeed Bookmark" entry
	 (file+headline nsavage-bookmarks-filename "To Read")
	 (function nsavage-get-elfeed-bookmark-template))
	("b" "Bookmark" entry
	 (file+headline nsavage-bookmarks-filename "To Read")
	 (function nsavage-get-bookmark-template))
	("x" "Exercise" entry
	 (file nsavage-exercise-filename)
	 (function nsavage-exercise-log-template))
	("i" "Inbox" entry
	 (file nsavage-inbox-filename)
	 (function nsavage-inbox-log-template))
	("l" "Log" plain
	 (file+function nsavage-logbook-filename nsavage-find-current-log)
	 "  %?\n")
	("t" "Todo" entry
	 (file+function nsavage-logbook-filename nsavage-find-current-logbook-date)
	 "* TODO %?\n")))

(setq org-clock-report-include-clocking-task t)

(setq send-mail-function 'sendmail-send-it
      sendmail-program "/usr/bin/msmtp"
      mail-specify-envelope-from t
      message-sendmail-envelope-from 'header
      mail-envelope-from 'header)

(defun nsavage-open-ssh-buffers ()
  "Open two vterm buffers to be used with my servers."
  (save-excursion
    (vterm)
    (rename-buffer "ssh-dione"))
  (save-excursion
    (vterm)
    (rename-buffer "ssh-athena")))

(nsavage-open-ssh-buffers)

(defun nsavage-open-financica-buffers ()
  (interactive)
  (save-excursion
    (vterm)
    (rename-buffer "run-financica-backend"))
  (save-excursion
    (vterm)
    (rename-buffer "run-financica-manage"))
  (save-excursion
    (vterm)
    (rename-buffer "run-financica-frontend")))

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (setq tab-always-indent t)
  (setq tab-width 8)
  (tide-hl-identifier-mode +1))

;; org latex export

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(android-mode-avd "AVD")
 '(android-mode-sdk-dir "/home/nick/Android/Sdk")
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#3c3836" "#fb4933" "#b8bb26" "#fabd2f" "#83a598" "#d3869b" "#8ec07c" "#ebdbb2"])
 '(custom-enabled-themes '(gruvbox-dark-soft))
 '(custom-safe-themes
   '("83e0376b5df8d6a3fbdfffb9fb0e8cf41a11799d9471293a810deb7586c131e6" "7661b762556018a44a29477b84757994d8386d6edee909409fabe0631952dad9" "123a8dabd1a0eff6e0c48a03dc6fb2c5e03ebc7062ba531543dfbce587e86f2a" "a06658a45f043cd95549d6845454ad1c1d6e24a99271676ae56157619952394a" "e1d09f1b2afc2fed6feb1d672be5ec6ae61f84e058cb757689edb669be926896" "939ea070fb0141cd035608b2baabc4bd50d8ecc86af8528df9d41f4d83664c6a" "aded61687237d1dff6325edb492bde536f40b048eab7246c61d5c6643c696b7f" "fe00bb593cb7b8c015bb2eafac5bfc82a9b63223fbc2c66eddc75c77ead7c7c1" "7a994c16aa550678846e82edc8c9d6a7d39cc6564baaaacc305a3fdc0bd8725f" "e1ef2d5b8091f4953fe17b4ca3dd143d476c106e221d92ded38614266cea3c8b" "b89a4f5916c29a235d0600ad5a0849b1c50fab16c2c518e1d98f0412367e7f97" default))
 '(httpd-host "0.0.0.0")
 '(org-babel-python-command "python3")
 '(org-confirm-babel-evaluate nil)
 '(package-selected-packages
   '(tide d-mode ox-rss android-mode kotlin-mode elfeed-org buttercup magit elfeed-web flycheck-pyflakes flycheck projectile use-package vterm counsel swiper ivy debbugs gruvbox-theme company markdown-mode evil))
 '(pdf-view-midnight-colors '("#fdf4c1" . "#282828"))
 '(yas-global-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

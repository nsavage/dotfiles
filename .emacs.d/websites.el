;;; Commentary: org publishing

;;; Code:

(defun nsavage-get-file-content (file)
  (with-temp-buffer
    (insert-file-contents file)
    (buffer-string)))

(defun nsavage-set-preamble ()
  (list `("en" ,(nsavage-get-file-content "~/code/website/portfolio/static/preamble.html"))))

(defun nsavage-set-postamble ()
  (list `("en" ,(nsavage-get-file-content "~/code/website/portfolio/static/postamble.html"))))

(setf org-html-preamble t)
(setf org-html-preamble-format (nsavage-set-preamble))

(setq org-image-actual-width nil)
(setf org-html-postamble t)
(setf org-html-postamble-format (nsavage-set-postamble))
(setq org-publish-project-alist
'(
  ("org-notes"
   :base-directory "~/code/website/portfolio"
   :base-extension "org"
   :publishing-directory "/ssh:nick@192.168.0.11#22222:/var/www/html"
   :recursive t
   :publishing-function org-html-publish-to-html
   :headline-levels 4             ; Just the default for this project.
   :style "<link rel=\"stylesheet\" href=\"static/stylesheet.css\" type=\"text/css\"/>"
   )
  ("org-static"
   :base-directory "~/code/website/portfolio/static"
   :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|svg"
   :publishing-directory "/ssh:nick@192.168.0.11#22222:/var/www/html/static"
   :recursive t
   :publishing-function org-publish-attachment)
  ("rss"
   :base-directory "~/code/website/portfolio"
   :base-extension "org"
   :publishing-directory "/ssh:nick@192.168.0.11#22222:/var/www/html"
   :html-link-home "http://nicksavage.ca"
   :html-link-use-abs-url t
   :rss-extension "xml"
   :exclude ".*"
   :include ("blog.org")
   ;; :exclude (regexp-opt '("index.org" "cv.org" "about.org" "projects.org"))
   :publishing-function org-rss-publish-to-rss
   )
  ("portfolio" :components ("org-notes" "org-static" "rss"))))

;(org-publish "portfolio" t)

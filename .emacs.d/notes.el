;;; notes.el --- Nick Savage's notes module

;; Copyright (C) 2021 Nick Savage

;; Author: Nick Savage (nick@nicksavage.ca)

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defun nsavage-get-elfeed-bookmark-template ()
  "Return org capture template with elfeed title, link and current date
FIXME: gracefully handle if elfeed-entry isn't open"
  (with-current-buffer "*elfeed-entry*"
    (let ((title (elfeed-entry-title elfeed-show-entry))
	  (link (elfeed-entry-link elfeed-show-entry)))
      (format "* %s\n%s\n%s" title link (format-time-string "%F")))))

(setq nsavage-bookmarks-filename "~/notes/special/2021-bookmarks.org")
(defun nsavage-get-bookmark-template ()
  "Return org capture template for a standard bookmark, without elfeed"
  (format "* \n\n%s" (format-time-string "%F")))

(setq nsavage-exercise-filename "~/notes/special/20210130182432-exercise_log.org")
(defun nsavage-exercise-log-template ()
  "Return org capture template for a standard bookmark, without elfeed"
  (format "* %s\nTime of day: \nTime: \nType:\n" (nsavage-get-current-date)))

(defun nsavage-inbox-log-template ()
  (format "* \n\n%s" (format-time-string "%F")))
;;; Functions to work with logbook.org

;; logbook.org is my note system, which I assume will live at /media/dione/nick/org-roam/logbook.org. Each
;; should open it if it isn't already open.
;; problem is that format-time-string pads string

(setq nsavage-logbook-filename "~/notes/special/logbook.org")
(setq nsavage-inbox-filename "~/notes/special/Inbox.org")
(setq nsavage-daily-note-location "~/notes/daily/")
(defun nsavage-get-daily-note-location ()
  (expand-file-name nsavage-daily-note-location))

(defun nsavage-get-inbox-location ()
  (expand-file-name nsavage-inbox-filename))

(defun nsavage-get-current-date ()
  "Return correctly formated current date"
  (format-time-string "%B %d, %Y"))

(nsavage-get-current-date)
(defun nsavage-get-tomorrow-date ()
  "Return correctly formatted date for tomorrow"

  (let* ((now (current-time))
	 (tomorrow (time-add now (* 24 3600))))
    (format-time-string "%B %d, %Y" tomorrow)))

(defun nsavage--open-file (file)
  "Open FILE as buffer, or switch to already opened buffer.
This checks first if current-buffer is the same as FILE. if they are
not, open FILE. If they are the same, no action is required."
  (if (not (eq (current-buffer) (get-file-buffer file)))
      (find-file-other-window file)))

(defun nsavage-open-logbook-file ()
  "Ensure logbook file is open."

  (let ((log nsavage-logbook-filename))
    (nsavage--open-file log)))

(defun nsavage-open-inbox-file ()
  "Ensure that inbox file is open."
  (interactive)
  (let ((inbox nsavage-inbox-filename))
    (nsavage--open-file inbox)))

(defun nsavage-get-date ()
  "Request date string from user"
  (let* ((current-date (nsavage-get-current-date))
	 (string (read-string (format "Get date (%s): " current-date) current-date)))
    (message string)))

(defun nsavage-open-given-logbook-date ()
  "Open logbook at user defined date"
  (interactive)
  (let ((date (nsavage-get-date)))
    (nsavage-open-log-date date)
    (org-end-of-subtree)))

(defun nsavage-open-current-logbook-date ()
  "Open logbook.org at today's date.
This is meant to use interactively and goes right to the end of the entry"
  (interactive)
  (let ((current-date (nsavage-get-current-date)))
    (nsavage-open-logbook-date current-date)
    (org-end-of-subtree)))

(defun nsavage-find-current-logbook-date ()
  
  (let ((current-date (nsavage-get-current-date)))
    (goto-char (point-min))
    (outline-show-all)
    (search-forward current-date)))

(defun nsavage-open-logbook-date (date)
  "Open logbook.org at given date.

This is meant to be used as part of other functions, and leaves the point where
it finds it after expanding the subtree"
  (nsavage-open-logbook-file)
  (goto-char (point-min))
  (outline-show-all)
  (search-forward date)
  (org-show-subtree))

(defun nsavage-open-current-todos ()
  "Show org sparse tree for all TODOs from today or before.
Assumes that logbook is arranged in a series of dates starting
September 1, 2020, September 2, 2020, etc. "
  (interactive)
  (let ((beg (point-min))
	(tomorrow (nsavage-get-tomorrow-date)))
    (nsavage-open-current-logbook-date)
    (call-interactively 'org-show-todo-tree)
    (save-excursion
      (search-forward tomorrow)
      (setq beg (point)))
    (nsavage-remove-future-highlights beg (point-max))))

(defun nsavage-remove-future-highlights (beg end)
  "Remove all highlights on org file between BEG and END"

  (unless org-inhibit-highlight-removal
    (org-overview)
    (setq temp-overlays '())
    (dolist (overlay org-occur-highlights)
      (let ((overlay-point (overlay-start overlay)))
	
	(if (and (> overlay-point beg) (< overlay-point end))
	    (delete-overlay overlay)
	  (progn
	    (save-excursion
	      (goto-char overlay-point)
	      (org-show-set-visibility 'ancestors)
	      (add-to-list 'temp-overlays overlay))))))
    (setq org-occur-highlights temp-overlays)))

(defun nsavage-find-current-log ()
  "Open log for current date in logbook.org"
  (interactive)
  (let ((current-date (nsavage-get-current-date)))
    (nsavage-open-logbook-date current-date)
  (search-forward "Log")
  (org-end-of-subtree)
  (insert "\n")))

(defun nsavage-open-daily-note ()
  "Opens a daily note file for the current date.
This replaces some of the functionality of org-roam."
  (interactive)
  (let ((current-date (format-time-string "%Y-%m-%d"))
	(location (nsavage-get-daily-note-location)))
    (find-file (concat location current-date ".org"))
    (if (not (search-forward "#+TITLE" nil t))
	(insert (concat "#+TITLE: " current-date "\n\n")))
    (goto-char (point-max))))

(defun nsavage-grep-notes-directory (regexp)
  "Wrapper around lgrep to search daily notes directory for REGEXP."
  (interactive "sSearch notes for: ")
  (lgrep regexp "*" (nsavage-get-daily-note-location)))

;;; notes.el ends here
